
## Personal Project

This project uses things that won't go on a basic Hubot installation, so it's for me only.

You can use it as a reference though for your own code. 

Feel free to steal the json if you want. I curated it from lists around the internet. If you modify the json for your own project, please do a pull request to add more options for this project! Thank you!
